;   BlinkLED's - Blink 3 LED's
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is part of BlinkLED's
;
;   Ths program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;==============================================
;BlinkLED's (2009 - Steven Rodriguez)
;==============================================

;General description
;=============================
; The leds are connected to:
; - Red led (RB1)
; - Green led (RB2)
; - Yellow led (RB3)
;=============================

;Assembler directives
;=============================
	list p=16f628a
	include p16f628a.inc
	__config b'11111100001001'
	org 0x00
	goto Start

;Functions
;=============================
	include digitecnology.inc
TurnRedLed
        bsf PORTB,1
        bcf PORTB,2
        bcf PORTB,3
        return
TurnGreenLed
        bcf PORTB,1
        bsf PORTB,2
        bcf PORTB,3
        return
TurnYellowLed
        bcf PORTB,1
        bcf PORTB,2
        bsf PORTB,3
	return
Delay
	movlw .156 ; 1
	movwf DELAY1 ; 1
	movlw .150 ; 1
	movwf DELAY2 ; 1
	movlw .14 ; 1
	movwf DELAY3 ; 1
	call delay3 ; 999717
	movlw .88 ; 1
	movwf DELAY1 ; 1
	call delay1 ; 269
	nop ; 1
	nop ; 1
	return ; 4

;Program
;=============================
Start
	goto Initialize
Initialize
	call chgbnk0 ; Go to bank 0	
	call chgbnk1 ; Go to bank 1
	bcf TRISB,1 ; Make RB1 output (0 = output, 1 = input) 
	bcf TRISB,2 ; Make RB2 output
	bcf TRISB,3 ; Make RB3 output
        call chgbnk0 ; Go to bank 0
	bcf PORTB,1
	bcf PORTB,2
	bcf PORTB,3
	goto Cycle
Cycle
	call TurnRedLed
	call Delay
	call TurnGreenLed
	call Delay
	call TurnYellowLed
	call Delay
	goto Cycle
	end
