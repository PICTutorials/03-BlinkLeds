/*
   BlinkLED's - Blink 3 LED's
   Copyright (C) 2009  Steven Rodriguez
   This program is part of BlinkLED's

   Ths program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   If this program has problems please contact me at:

   stevencrc@digitecnology.zapto.org
*/

//================================================
//PIC DELAY CALCULATION
//                      (2009 - Steven Rodriguez)
//================================================

/*====================================
General equations (in CPU cycles):

Delay 1:

	((DELAY1 - 1) * 3) + 4 + 2 + 2

Delay 2:

	(DELAY1_CYCLES * DELAY2) + ((DELAY2 - 1) * 3) + 4 + 2 + 2

Delay 3:

	(DELAY2_CYCLES * DELAY3) + ((DELAY3 - 1) * 3) + 4 + 2 + 2

CPU clock time:

	1 / F

CPU Instruction time (cycle time):

	(1 / F) * 4

=====================================*/

#include <stdio.h>

//Enumerations
enum DelayType
{
	NANOSECONDS = 0,
	MICROSECONDS = 1,
	MILLISECONDS = 2,
	SECONDS = 3,
	MINUTES = 4,
	HOURS = 5
};

//Global Variables
unsigned char delay1;
unsigned char delay2;
unsigned char delay3;
int delaytype;
int delayvalue;
long crystalclock;
long long sparta;

/* Main entry */
int main()
{	
	printf("====================================\n");
	printf("Welcome to the PIC delay assistant.\n");
	printf("====================================\n\n");
/*
	//Crystal clock selection
	printf("Select the frecuency in Hz of the oscillator:\n\n");
	printf("Value:");

	if(scanf("%i",&crystalclock) <= 0)
	{
		printf("WTF!\n");
		return -1;
	}
		
	printf("\n");

	//Delay type selection
	printf("Select a time type to delay:\n\n");
	printf("1) Nanoseconds\n");
	printf("2) Microseconds\n");
	printf("3) Milliseconds\n");
	printf("4) Seconds\n");
	printf("5) Minutes\n");
	printf("6) Hours\n\n");
	printf("Value: ");

	if(scanf("%i",&delaytype) <= 0)
	{
		printf("WTF!\n");
		return -1;
	}	
	
	delaytype -= 1;

	printf("\n");

	//Delay value selection
	printf("Select the delay value:");
	
	if(scanf("%i",&delayvalue) <= 0)
        {
                printf("WTF!\n");
                return -1;
        }

	printf("\n");
*/
/* TEST PURPOSE */
	//Insert delay1 value
	printf("Delay1:");

	if(scanf("%i",&delay1) <= 0)
        {
                printf("WTF!\n");
                return -1;
        }

	printf("\n");

	//Insert delay2 value
        printf("Delay2:");

        if(scanf("%i",&delay2) <= 0)
        {
                printf("WTF!\n");
                return -1;
        }

        printf("\n");

	//Insert delay3 value
        printf("Delay3:");

        if(scanf("%i",&delay3) <= 0)
        {
                printf("WTF!\n");
                return -1;
        }

	sparta = ((((((delay1 - 1) * 3) + 8) * delay2) + ((delay2 - 1) * 3) + 8) * delay3) + ((delay3 - 1) * 3) + 8;

        printf("\n");

	printf("Result: %i cycles\n",sparta);	
	
	return 0;
}


